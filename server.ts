import {default as express, static as _static} from 'express';
import {readFile} from 'fs';

const PORT = process.env.PORT || 8080

interface IPack {
  [name: string]: string;
}

interface packI{
  Package: string,
  Description: string,
  Provides: string[] | string,
  Depends: string[] | string
  Dependents: string[]
}

const getDataFromStatus = (data:Buffer, depth:string[]):any => {
  let value = 
  data.toString()
    .split("\n\n")
    .map((value)=>{return value.split(/(?!\n )(\n)/g)}) //matches newline but not newline followed by space
    .map((arr)=>{return arr.map((pars)=>{
      if(!pars.includes(': ')){return} //skip lines without 
      let f:IPack = {};
      let b = pars.split(': ')
      let a = b.shift()
      f[a!.trim()] = b.length-1? b.reduce((acc,val)=>{return acc+':'+val}): b[0].trim();
      return f
      })
      .filter((supe)=>{return supe !== undefined})
      .filter((propers)=>{
        let [a] = Object.keys(propers!)
        return depth.includes(a)
      })
    })
    .map((arr)=>{return Object.assign({}, ...arr)})
  value.pop() //remove trailing value
  return value
}

const app = express();

app.use(_static(__dirname+"static/"));
app.get("/",(req,res)=>{
  res.sendFile(__dirname+"/static/index.html");
})
app.get("/index",(req,res)=>{
  readFile(process.platform === "linux"? "/var/lib/dpkg/status":"status",(err, data) => {
    let packArr: Array<packI> = getDataFromStatus(data,["Package", "Description"])
    
    res.status(200)
    res.json(JSON.stringify(packArr))
  });
});

app.get("/:package",(req,res)=>{
  res.status(200);
  res.sendFile(__dirname+"/static/package.html")
});

app.get("/package/:package", (req,res)=>{
  let { params } = req

  readFile("status",(err, data) => {
    let packArr: Array<packI> = getDataFromStatus(data,["Package", "Description", "Depends", "Provides"])
    //remap the array to find to prepare for dependents
    packArr.map((deps)=>{
     // Note: Pre dependencies are a thing to consider as well, but I decided not to parse them here because, technically those don't belong in the dependency position on the table
     if(deps['Depends']){
       deps['Depends'] = (deps['Depends'] as string).replace(/\(.+?\)/g, "").split(/\,|\|/g).map((str:string)=>{return str.trim()})
     }
     if(deps['Provides']){
       deps['Provides'] = (deps['Provides'] as string).replace(/\(.+?\)/g, "").split(/\,|\|/g).map((str:string)=>{return str.trim()})
     }
     return deps

   }).map(pack => {
      if(pack['Depends']){
        pack['Depends'] = (pack['Depends'] as string[]).map((dependency)=>{
          let dependent = packArr.find(({ Package, Provides })=>{
            return Package === dependency || (Provides? Provides.includes(dependency): false)
          });
          if(!dependent){return dependency};
          if(!dependent.hasOwnProperty("Dependents")){dependent['Dependents'] = [ pack['Package'] ]}
          else if(!dependent['Dependents'].includes(pack['Package'])){dependent['Dependents'].push(pack['Package'])};
          
          return dependent['Package']
       }).filter((v,i,a)=>a.indexOf(v)===i);
     };
     return pack;
    });
    let pack = packArr.find((elem)=>{
      return elem['Package'] == params['package']
    })
    if(!pack){
      res.status(404);
      res.json({error:"Not Founds"})
    }else{
      res.status(200);
      res.json(JSON.stringify(pack))
    }
  });
});

app.listen(PORT, ()=>{});