#DPKG-LS

This is an HTML interface for listing installed packages on ubuntu-like platforms.

To run you will need to install the dependencies with `npm i` and then compile either with your global tsc installation or use `./node_modules/typescript/bin/tsc`.

Finally to run the server run `node server.js` this will spawn a webserver on `localhost`

I've provided an example file for windows installations, for other linux based systems that dont use tha apt package installer copy the status file to `/var/lib/dpkg/status`

Example website: [https://dpkg-ls.herokuapp.com/](https://dpkg-ls.herokuapp.com/) heres the app running on heroku